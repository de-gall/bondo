// import { Injectable } from '@angular/core';
// import {
//   Auth,
//   createUserWithEmailAndPassword,
//   signInWithEmailAndPassword,
//   signOut,
//   user,
// } from '@angular/fire/auth';

// @Injectable({
//   providedIn: 'root',
// })
// export class AuthService {
  
//   constructor(private auth: Auth) {}
//   async register(email :any, password:any) {
//     try {
//       createUserWithEmailAndPassword(this.auth, email, password);
//       return user;
//     } catch (e) {
//       return null;
//     }
//   }
//   async logIn(email: any, password: string) {
//     try {
//       signInWithEmailAndPassword(this.auth, email, password);
//       return user;
//     } catch (e) {
//       return null;
//     }
//   }
//   logOut(){
//     signOut(this.auth);
//   };
  
// }
import { Injectable } from '@angular/core';
import { Auth, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, User } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  
  constructor(private auth: Auth) {}

  async register(email: string, password: string): Promise<User | null> {
    try {
      const userCredential = await createUserWithEmailAndPassword(this.auth, email, password);
      return userCredential.user;
    } catch (e) {
      console.error('Error during registration:', e);
      return null;
    }
  }

  async logIn(email: string, password: string): Promise<User | null> {
    try {
      const userCredential = await signInWithEmailAndPassword(this.auth, email, password);
      return userCredential.user;
    } catch (e) {
      console.error('Error during login:', e);
      return null;
    }
  }

  logOut(): void {
    signOut(this.auth);
  }
}