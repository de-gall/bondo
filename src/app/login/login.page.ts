import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, NavController, NavParams } from '@ionic/angular';
import { AuthService } from '../service/authService';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  credentials: any; 
  inputemail:string='';
  inputpassword:string='';
  myGroup: any;

	constructor(
		private fb: FormBuilder,
		private loadingController: LoadingController,
		private alertController: AlertController,
		@Inject('authService') private authService: AuthService,
		private router: Router
	) {}

  get email() {
		return this.credentials.get('email');
	}

	get password() {
		return this.credentials.get('password');
	}

  ngOnInit() {
   // this.inputemail=Validators.email
		this.credentials = this.fb.group({
			email: ['', [Validators.required, Validators.email]],
			password: ['', [Validators.required, Validators.minLength(6)]]
		});
	}
  async register() {
		const loading = await this.loadingController.create();
		await loading.present();

		const user = await this.authService.register(this.credentials.get('email'),this.credentials.get('password'));
		await loading.dismiss();

		if (user) {
			this.router.navigateByUrl('/home', { replaceUrl: true });
		} else {
			this.showAlert('Registration failed', 'Please try again!');
		}
	}

	async login() {
	// 	const loading = await this.loadingController.create();
	// 	await loading.present();

	// 	const user = await this.authService.logIn(this.credentials.value);
	// 	await loading.dismiss();

	// 	if (user) {
	// 		this.router.navigateByUrl('/home', { replaceUrl: true });
	// 	} else {
	// 		this.showAlert('Login failed', 'Please try again!');
	// 	}
	 }

	async showAlert(header: string, message: string) {
		const alert = await this.alertController.create({
			header,
			message,
			buttons: ['OK']
		});
		await alert.present();
	}
}
