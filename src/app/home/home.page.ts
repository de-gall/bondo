import { Component } from '@angular/core';
import { NgControl } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { DaniComponent } from '../components/dani/dani.component';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [NavController,]

})
export class HomePage {
 
  options = {
    centeredSlides: true,
    slidesPerView: 1,
    spaceBetween: -60,
  };

  categories = {
    slidesPerView: 2.5,
  };
  constructor(public navCont :NavController) {}
  lecliks() {
    this.navCont.navigateForward('/testelog')
    console.log('this is the name : ',)
  }

}
