import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-liste-evenement',
  templateUrl: './liste-evenement.page.html',
  styleUrls: ['./liste-evenement.page.scss'],
})
export class ListeEvenementPage implements OnInit {

  public alertButtons = ['OK'];
  public alertInputs = [
    {
      placeholder: 'Name',
    },
    {
      type: 'date',
      placeholder: 'Date',
    },
    {
      type: 'textarea',
      placeholder: 'Lieux',
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
