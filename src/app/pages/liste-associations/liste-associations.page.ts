import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-liste-associations',
  templateUrl: './liste-associations.page.html',
  styleUrls: ['./liste-associations.page.scss'],
})
export class ListeAssociationsPage implements OnInit {

  constructor(public navCont :NavController) { }

  ngOnInit() {
  }
  public actionSheetButtons = [
    {
      text: 'Delete',
      role: 'destructive',
      data: {
        action: 'delete',
      },
    },
    {
      text: 'Share',
      data: {
        action: 'share',
      },
    },
    {
      text: 'Cancel',
      role: 'cancel',
      data: {
        action: 'cancel',
      },
    },
  ];
  focusSearch() {
    const searchBar = document.getElementById('searchBar') as HTMLIonSearchbarElement;
    if (searchBar) {
      searchBar.setFocus();
    }
  }

  goTo() {
    this.navCont.navigateForward('/creation-association')
    console.log('this is the name : ',)
  }
}
