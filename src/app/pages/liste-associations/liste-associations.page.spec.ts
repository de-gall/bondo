import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListeAssociationsPage } from './liste-associations.page';

describe('ListeAssociationsPage', () => {
  let component: ListeAssociationsPage;
  let fixture: ComponentFixture<ListeAssociationsPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ListeAssociationsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
