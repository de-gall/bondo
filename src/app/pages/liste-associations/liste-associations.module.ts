import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListeAssociationsPageRoutingModule } from './liste-associations-routing.module';

import { ListeAssociationsPage } from './liste-associations.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListeAssociationsPageRoutingModule
  ],
  declarations: [ListeAssociationsPage]
})
export class ListeAssociationsPageModule {}
