import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {

  monFormulaire: FormGroup;
  constructor( public toastController:ToastController, private formBuilder: FormBuilder,public navCont: NavController) {
    this.monFormulaire = this.formBuilder.group({
      rNom: ['', Validators.required],
      rPrenom: ['', Validators.required],
      rDate: ['', Validators.required],
      rSexe: ['', Validators.required],
      rAdresse: ['', Validators.required],
      rTEL: ['', Validators.required],
      rEmail: ['', [Validators.required, Validators.email]],
      rMDP: ['', Validators.required],
      rMDPC: ['', Validators.required],
      rStatut: ['', Validators.required],
      rMDTP: ['', [Validators.required, Validators.minLength(6)]],
      rMDTPC: ['', [Validators.required, Validators.minLength(6)]],
      
    });
  }
   submitForm() {
    console.log('Nom:', this.monFormulaire.get('rNom')?.value);
    console.log('Statut:', this.monFormulaire.get('rStatut')?.value);
    
    
    if (this.monFormulaire.valid) {
      // Soumettre le formulaire
      // this.navCont.navigateForward('/liste-associations')
    } else {
     
      this.presentToast();
  
      // Afficher des messages d'erreur
    }
  }
  async presentToast() {
     const toast = await this.toastController.create({
      message: 'Remplisser correctement le formulaire!',
      duration: 1500,
      position: 'bottom',
      color: 'danger',
      buttons :[{
        text:'Fermé',
        role: 'cancel',
      }]
    });

    await toast.present();
  }
  ngOnInit() {
  }

}
