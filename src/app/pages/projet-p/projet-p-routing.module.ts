import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjetPPage } from './projet-p.page';

const routes: Routes = [
  {
    path: '',
    component: ProjetPPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjetPPageRoutingModule {}
