import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PorteMonnaiePage } from './porte-monnaie.page';

describe('PorteMonnaiePage', () => {
  let component: PorteMonnaiePage;
  let fixture: ComponentFixture<PorteMonnaiePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(PorteMonnaiePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
