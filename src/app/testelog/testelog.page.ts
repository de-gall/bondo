import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-testelog',
  templateUrl: './testelog.page.html',
  styleUrls: ['./testelog.page.scss'],
})
export class TestelogPage implements OnInit {
  
  public alertButtons = ['VLIDER'];
   inputValue1: any;
    inputValue2: any;
  csssClass= 'my-custom-alert'
  public alertInputs = [
    {
      type: 'nomber',
      placeholder: 'Numero de tel',
      attribut:{
        maxlength:9
      }
    },
    {
      placeholder: 'Nickname (max 8 characters)',
      attributes: {
        maxlength: 8,
      },
    },
    {
      type: 'password',
      placeholder: '******',
      maxlength: 9,
    },
  ];
  
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Mot de passe oublier',
      message: 'Entrez soit le Mail ou le Numero de téléphone',
      inputs: [
        {
          name: 'input2',
          type: 'email',  
          placeholder: 'Mail',
        },
        {
          name: 'num1',
          type: 'number',
          placeholder: 'Numero de tel',

        }
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Opération annulée');
          }
        }, {
          text: 'OK',
          handler: (data) => {
            this.inputValue1 = data.num1;
           this.inputValue2 = data.input2;
            // Utilisez les valeurs des champs de saisie ici
            console.log('Valeur du champ de saisie 1 :', data.num1);
            console.log('Valeur du champ de saisie 2 :', data.input2);
          }
        }],
    });

    await alert.present();
  }
  async logByPhon() {
    const alert = await this.alertController.create({
      header: 'log par Numero',
      inputs: [
        {
          name: 'phoneLog',
          type: 'number',  
          placeholder: 'Numero de tel',
         
        },
        {
          name: 'PhonePass',
          type: 'password',
          placeholder: '******',
          attributes:{
            maxlength:9
          }
        }
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Opération annulée');
          }
        }, {
          text: 'OK',
          handler: (data) => {
            console.log(data);
            
            this.inputValue1 = data.phoneLog;

            this.inputValue2 = data.PhonePass;
            // Utilisez les valeurs des champs de saisie ici
            console.log('Valeur du champ de saisie 1 :', data.PhonePass);
            console.log('Valeur du champ de saisie 2 :', data.phoneLog);
          }
        }],
    });

    await alert.present();
  }
  // Désactiver le champ 1 lorsqu'il est en cours de saisie


handleClick() {
throw new Error('Method not implemented.');
}

  staySignedIn: boolean = true;
  monFormulaire: FormGroup;

  constructor(private formBuilder: FormBuilder,public navCont: NavController,private alertController: AlertController) {
    this.monFormulaire = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  submitForm() {
    
    if (this.monFormulaire.valid) {
      // Soumettre le formulaire
      this.navCont.navigateForward('/liste-associations')
    } else {
      // Afficher des messages d'erreur
    }
  }
  ngOnInit() {}

  // changeStatus(value:any) {
  //   this.staySignedIn = value;
  // }
  lecliks() {
    this.navCont.navigateForward('/inscription')
    console.log('this is the name : ',)
  }
  validLog() {
    this.navCont.navigateForward('/liste-associations')
    console.log('this is the name : ',)
  }
 


}
