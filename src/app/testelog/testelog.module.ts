import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestelogPageRoutingModule } from './testelog-routing.module';

import { TestelogPage } from './testelog.page';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    TestelogPageRoutingModule,
    ReactiveFormsModule 
  ],
  declarations: [TestelogPage]
})
export class TestelogPageModule {}
