import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'testelog',
    loadChildren: () => import('./testelog/testelog.module').then( m => m.TestelogPageModule)
  },
  {
    path: 'inscription',
    loadChildren: () => import('./pages/inscription/inscription.module').then( m => m.InscriptionPageModule)
  },
  {
    path: 'liste-associations',
    loadChildren: () => import('./pages/liste-associations/liste-associations.module').then( m => m.ListeAssociationsPageModule)
  },
  {
    path: 'activite',
    loadChildren: () => import('./pages/assocPages/activite/activite.module').then( m => m.ActivitePageModule)
  },
  {
    path: 'tab-p',
    loadChildren: () => import('./tabs/tab-p/tab-p.module').then( m => m.TabPPageModule)
  },
  {
    path: 'tab-activite',
    loadChildren: () => import('./tabs/tab-activite/tab-activite.module').then( m => m.TabActivitePageModule)
  },
  {
    path: 'tab-accueil',
    loadChildren: () => import('./tabs/tab-accueil/tab-accueil.module').then( m => m.TabAccueilPageModule)
  },
  {
    path: 'tab-comm',
    loadChildren: () => import('./tabs/tab-comm/tab-comm.module').then( m => m.TabCommPageModule)
  },
  {
    path: 'tab-autre',
    loadChildren: () => import('./tabs/tab-autre/tab-autre.module').then( m => m.TabAutrePageModule)
  },
  {
    path: 'creation-association',
    loadChildren: () => import('./pages/creation-association/creation-association.module').then( m => m.CreationAssociationPageModule)
  },
  {
    path: 'liste-projet',
    loadChildren: () => import('./assocPages/liste-projet/liste-projet.module').then( m => m.ListeProjetPageModule)
  },
  {
    path: 'liste-evenement',
    loadChildren: () => import('./assocPages/liste-evenement/liste-evenement.module').then( m => m.ListeEvenementPageModule)
  },
  {
    path: 'liste-notification',
    loadChildren: () => import('./assocPages/liste-notification/liste-notification.module').then( m => m.ListeNotificationPageModule)
  },
  {
    path: 'a-propos',
    loadChildren: () => import('./pages/aPropCondiSignal/a-propos/a-propos.module').then( m => m.AProposPageModule)
  },
  {
    path: 'condition',
    loadChildren: () => import('./pages/aPropCondiSignal/condition/condition.module').then( m => m.ConditionPageModule)
  },
  {
    path: 'edit-profile',
    loadChildren: () => import('./pages/edit-profile/edit-profile.module').then( m => m.EditProfilePageModule)
  },
  {
    path: 'porte-monnaie',
    loadChildren: () => import('./pages/porte-monnaie/porte-monnaie.module').then( m => m.PorteMonnaiePageModule)
  },
  {
    path: 'liste-dons',
    loadChildren: () => import('./assocPages/liste-dons/liste-dons.module').then( m => m.ListeDonsPageModule)
  },

  {
    path: 'caisse',
    loadChildren: () => import('./pages/caisse/caisse.module').then( m => m.CaissePageModule)
  },
  {
    path: 'liste-documents',
    loadChildren: () => import('./assocPages/liste-documents/liste-documents.module').then( m => m.ListeDocumentsPageModule)
  },
  {
    path: 'projet-p',
    loadChildren: () => import('./pages/projet-p/projet-p.module').then( m => m.ProjetPPageModule)
  },
  
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
