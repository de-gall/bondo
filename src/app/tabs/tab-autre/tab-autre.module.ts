import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabAutrePageRoutingModule } from './tab-autre-routing.module';

import { TabAutrePage } from './tab-autre.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabAutrePageRoutingModule
  ],
  declarations: [TabAutrePage]
})
export class TabAutrePageModule {}
