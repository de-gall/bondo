import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TabAccueilPage } from './tab-accueil.page';

describe('TabAccueilPage', () => {
  let component: TabAccueilPage;
  let fixture: ComponentFixture<TabAccueilPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(TabAccueilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
