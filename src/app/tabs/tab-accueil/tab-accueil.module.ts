import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabAccueilPageRoutingModule } from './tab-accueil-routing.module';
import { TabAccueilPage } from './tab-accueil.page';
import { DaniComponent } from 'src/app/components/dani/dani.component';
import { CompAccueilComponent } from 'src/app/components/comp-accueil/comp-accueil.component'; 

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabAccueilPageRoutingModule,
    DaniComponent,
  ],
  declarations: [TabAccueilPage,
    CompAccueilComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA,]

})
export class TabAccueilPageModule {}
