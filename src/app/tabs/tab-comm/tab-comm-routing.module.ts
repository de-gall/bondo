import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabCommPage } from './tab-comm.page';

const routes: Routes = [
  {
    path: '',
    component: TabCommPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabCommPageRoutingModule {}
