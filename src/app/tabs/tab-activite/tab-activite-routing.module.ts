import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabActivitePage } from './tab-activite.page';

const routes: Routes = [
  {
    path: '',
    component: TabActivitePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabActivitePageRoutingModule {}
