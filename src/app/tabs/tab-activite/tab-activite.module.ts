import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabActivitePageRoutingModule } from './tab-activite-routing.module';

import { TabActivitePage } from './tab-activite.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabActivitePageRoutingModule
  ],
  declarations: [TabActivitePage]
})
export class TabActivitePageModule {}
